﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Autofac;
using GalaSoft.MvvmLight;
using Microsoft.Practices.ServiceLocation;
using PushContractsToRabbitMQ.RmqEnvironment;
using PushContractsToRabbitMQ.RmqEnvironment.Iface;
using PushContractsToRabbitMQ.ViewModel;
using PushContractsToRabbitMQ.ViewModel.Iface;

namespace PushContractsToRabbitMQ
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            AppDomain.CurrentDomain.UnhandledException += ListCurrentDomainOnUnhandledException;
        }

        private void ListCurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //TODO: Обработать искючение
            MessageBox.Show(App.Current.MainWindow, $"App crash: {Environment.NewLine}{e.ExceptionObject}", "App Crash", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        /// <summary>
        /// Initialize IoC environment
        /// </summary>
        /// <param name="e">args</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            ServiceLocator.SetLocatorProvider(() => AutofacServiceLocator.Instance);
            var builder = AutofacServiceLocator.Instance.GetBuilder();
            builder.RegisterType<EndpointStore>().As<IEndpointStore>().SingleInstance();
            builder.RegisterType<RmqPublishViewModel>().As<IRmqPublishViewModel>().As<ViewModelBase>().SingleInstance();
            AutofacServiceLocator.Instance.ActivateIoc();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            ViewModelLocator.Cleanup();
            base.OnExit(e);
        }
    }


}
