﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using PushContractsToRabbitMQ.RmqEnvironment.Model;
using PushContractsToRabbitMQ.ViewModel.Model;

namespace PushContractsToRabbitMQ.ViewModel.Iface
{
    /// <summary>
    /// Интерфейс вью модели публикации контрактов в очереди RabbitMQ
    /// </summary>
    public interface IRmqPublishViewModel
    {
        /// <summary>
        /// Список очередей рэббита
        /// </summary>
        ObservableCollection<RmqEndpointViewModelItem> EndpointList { get; }

        /// <summary>
        /// Список контрактов
        /// </summary>
        ObservableCollection<ContractItem> ContractItems { get; }

        /// <summary>
        /// Имя хоста броке сообщений
        /// </summary>
        string HostName { get; set; }

        /// <summary>
        /// Виртуальный хост брокера
        /// </summary>
        string VirtualHost { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        string Password { get; set; }

        /// <summary>
        /// Расширение файла
        /// </summary>
        string DllPath { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        string IdField { get; set; }

        /// <summary>
        /// Фильтр списка контрактов
        /// </summary>
        string ContractFilter { get; set; }

        /// <summary>
        /// Сериализованный контракт JSON
        /// </summary>
        string JsonContractText { get; set; }

        /// <summary>
        /// Выбранный контракт
        /// </summary>
        ContractItem SelectedContract { get; set; }

        /// <summary>
        /// Реализация команды отправки сообщения в очередь
        /// </summary>
        ICommand PublishMessageCmd { get; }

        /// <summary>
        /// Обзор папок со сборками
        /// </summary>
        ICommand BrowseDllsCmd { get; }

        /// <summary>
        /// Сохранить конечные точкиы
        /// </summary>
        ICommand StoreEndpointsCmd { get; }
    }
}
