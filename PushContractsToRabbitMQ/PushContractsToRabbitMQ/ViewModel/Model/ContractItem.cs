﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PushContractsToRabbitMQ.ViewModel.Model
{
    public class ContractItem
    {
        public ContractItem(TypeInfo typeInfo)
        {
            TypeInfo = typeInfo;
        }

        /// <summary>
        /// Тип контракта
        /// </summary>
        public TypeInfo TypeInfo { get; private set; }

        /// <summary>
        /// Имя контракта
        /// </summary>
        public string ContractName => TypeInfo?.Name;

        /// <summary>
        /// Полное имя типа контракта
        /// </summary>
        public string FullName => string.Format("{0}, {1}", TypeInfo?.FullName, TypeInfo?.AssemblyQualifiedName);
    }
}
