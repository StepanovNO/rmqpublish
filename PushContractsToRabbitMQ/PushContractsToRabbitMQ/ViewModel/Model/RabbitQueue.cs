﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PushContractsToRabbitMQ.ViewModel.Model
{
    /// <summary>
    /// Очередь рэббита
    /// </summary>
    public class RabbitQueue
    {
        public RabbitQueue()
        {
            
        }

        public RabbitQueue(string queueName)
        {
            QueueName = queueName;
        }

        /// <summary>
        /// Имя очереди
        /// </summary>
        public string QueueName { get; set; }
    }
}
