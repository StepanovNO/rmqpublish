﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PushContractsToRabbitMQ.ViewModel.Model
{
    /// <summary>
    /// Эксчейнджер RabbitMQ
    /// </summary>
    public class RabbitExchangeItem
    {
        public RabbitExchangeItem()
        {
            
        }

        public RabbitExchangeItem(string exchangerName)
        {
            ExchangerName = exchangerName;
        }

        /// <summary>
        /// Имя эксчейнджера
        /// </summary>
        public string ExchangerName { get; set; }
    }
}
