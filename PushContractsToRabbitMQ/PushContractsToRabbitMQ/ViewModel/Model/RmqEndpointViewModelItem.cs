﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using PushContractsToRabbitMQ.RmqEnvironment.Model;

namespace PushContractsToRabbitMQ.ViewModel.Model
{
    public class RmqEndpointViewModelItem : ViewModelBase
    {
        private readonly RmqEndpoint _ep;
        private bool _isDirty;

        public RmqEndpointViewModelItem(RmqEndpoint ep)
        {
            _ep = ep ?? throw new ArgumentNullException(nameof(ep));
            IsDirty = false;
        }

        public RmqEndpointViewModelItem()
        {
            _ep = new RmqEndpoint();
            IsDirty = true;
        }

        /// <summary>
        /// Новая запись
        /// </summary>
        public bool IsDirty
        {
            get => _isDirty;
            set
            {
                _isDirty = value;
                RaisePropertyChanged(() => IsDirty);
            }
        }

        /// <summary>
        /// Модель точки подключения
        /// </summary>
        public RmqEndpoint Model => _ep;

        /// <summary>
        /// Имя кончной точки
        /// </summary>
        public string EndpointName
        {
            get => _ep.EndpointName;
            set
            {
                _ep.EndpointName = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// Тип конечной точки
        /// </summary>
        public EndPointType EndpointType
        {
            get => _ep.EndpointType;
            set
            {
                _ep.EndpointType = value;
                IsDirty = true;
            }
        }
    }
}
