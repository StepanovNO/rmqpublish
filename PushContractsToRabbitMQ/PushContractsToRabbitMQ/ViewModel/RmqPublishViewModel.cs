using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Newtonsoft.Json;
using PushContractsToRabbitMQ.RmqEnvironment.Iface;
using PushContractsToRabbitMQ.RmqEnvironment.Model;
using PushContractsToRabbitMQ.ViewModel.Iface;
using PushContractsToRabbitMQ.ViewModel.Model;

namespace PushContractsToRabbitMQ.ViewModel
{

    /// <summary>
    /// ��������� �������� ���� ����������
    /// </summary>
    public class RmqPublishViewModel : ViewModelBase, IRmqPublishViewModel
    {
        private readonly IEndpointStore _epStore;
        private string _login;
        private string _password;
        private string _hostName;
        private string _dllPath;
        private string _contractFilter;
        private ContractItem[] _currentContracts;
        private ContractItem _selectedContract;
        private string _jsonContractText;
        private ICommand _storeEndpoints;

        public RmqPublishViewModel(IEndpointStore epStore)
        {
            _epStore = epStore ?? throw new ArgumentNullException(nameof(epStore));
            EndpointList = new ObservableCollection<RmqEndpointViewModelItem>();
            EndpointList.CollectionChanged += ListEndpointListOnCollectionChanged;
            ContractItems = new ObservableCollection<ContractItem>();
            BrowseDllsCmd = new RelayCommand(InvokeBrowseDlls);
            PublishMessageCmd = new RelayCommand(PublishMessage);
            StoreEndpointsCmd  = new RelayCommand(StoreEndpoints);
            VirtualHost = "\\";
            Init();
        }

        /// <summary>
        /// ��������� �������� ����� �����������
        /// </summary>
        public ObservableCollection<RmqEndpointViewModelItem> EndpointList { get; }

        /// <summary>
        /// ������ ����������
        /// </summary>
        public ObservableCollection<ContractItem> ContractItems { get; }

        /// <summary>
        /// ��� ����� ����� ���������
        /// </summary>
        public string HostName
        {
            get => _hostName;
            set
            {
                _hostName = value;
                RaisePropertyChanged(() => HostName);
            } 
        }

        /// <summary>
        /// ����������� ���� �������
        /// </summary>
        public string VirtualHost { get; set; }

        /// <summary>
        /// �����
        /// </summary>
        public string Login
        {
            get => _login;
            set
            {
                _login = value;
                RaisePropertyChanged(() => Login);
            }
        }

        /// <summary>
        /// ������
        /// </summary>
        public string Password
        {
            get => _password;
            set => _password = value;
        }

        /// <summary>
        /// ���������� �����
        /// </summary>
        public string DllPath
        {
            get => _dllPath;
            set
            {
                _dllPath = value;
                RaisePropertyChanged(() => DllPath);
            }
        }

        /// <summary>
        /// ������ ��������� ������
        /// </summary>
        public string ContractFilter
        {
            get => _contractFilter;
            set
            {
                _contractFilter = value;
                BuilderTypesWithFilter();
                RaisePropertyChanged(() => ContractFilter);
            }
        }

        /// <summary>
        /// ��������������� �������� JSON
        /// </summary>
        public string JsonContractText
        {
            get => _jsonContractText;
            set
            {
                _jsonContractText = value;
                RaisePropertyChanged(() => JsonContractText);
            }
        }

        /// <summary>
        /// ��������� ��������
        /// </summary>
        public ContractItem SelectedContract
        {
            get => _selectedContract;
            set
            {
                _selectedContract = value;
                RaisePropertyChanged(() => SelectedContract);
                DisplayJsonContract(_selectedContract);
            }
        }

        /// <summary>
        /// Id
        /// </summary>
        public string IdField { get; set; }

        /// <summary>
        /// ���������� ������� �������� ��������� � �������
        /// </summary>
        public ICommand PublishMessageCmd { get; }

        /// <summary>
        /// ����� ����� �� ��������
        /// </summary>
        public ICommand BrowseDllsCmd { get; }

        /// <summary>
        /// ��������� �������� ������
        /// </summary>
        public ICommand StoreEndpointsCmd { get; }


        /// <summary>
        /// ��������� �������� ����� �����������
        /// </summary>
        private void StoreEndpoints()
        {
            foreach (var rmqEndpoint in EndpointList.Where(ep => ep.IsDirty))
            {
                rmqEndpoint.IsDirty = false;
                _epStore.StoreEndpoint(rmqEndpoint.Model);
            }
            
        }

        /// <summary>
        /// ���������� ���������� ��������� � RabbitMQ
        /// </summary>
        private void PublishMessage()
        {

        }

        /// <summary>
        /// ������������� �������� ���� � JSON
        /// </summary>
        private void DisplayJsonContract(ContractItem item)
        {
            if (item == null)
            {
                JsonContractText = "Select contract";
                return;
            }

            try
            {
                var instance = Activator.CreateInstance(item.TypeInfo);
                JsonContractText = JsonConvert.SerializeObject(instance, Formatting.Indented, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All,
                    FloatParseHandling = FloatParseHandling.Decimal,
                    Formatting = Formatting.Indented
                });
            }
            catch (Exception ex)
            {
                JsonContractText = ex.ToString();
            }
        }

        /// <summary>
        /// ������� ������ ��������� ������
        /// </summary>
        private void InvokeBrowseDlls()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Dll files (.Dll)|*.Dll";
            var result = dlg.ShowDialog();

            if (result == true)
            {
                DllPath = dlg.FileName;
                try
                {
                    var assembly = Assembly.LoadFrom(DllPath);
                    _currentContracts = assembly
                        .DefinedTypes
                        .Select(t => new ContractItem(t))
                        .OrderBy(c => c.ContractName)
                        .ToArray();
                    BuilderTypesWithFilter();
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Exception occured while loading {dlg.FileName}:{Environment.NewLine}{ex}");
                }
            }
        }

        private void ListEndpointListOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void BuilderTypesWithFilter()
        {
            ContractItems.Clear();
            if (!string.IsNullOrEmpty(ContractFilter) )
            {
                foreach (var currentContract in _currentContracts)
                {
                    if (Regex.Match(currentContract.ContractName, ContractFilter, RegexOptions.IgnoreCase).Success)
                    {
                        ContractItems.Add(currentContract);
                    }
                }
            }
            else
            {
                foreach (var currentContract in _currentContracts)
                {
                    ContractItems.Add(currentContract);
                }
            }
        }

        public override void Cleanup()
        {
            Configuration currentConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            currentConfig.AppSettings.Settings["Dll"].Value = DllPath;
            currentConfig.AppSettings.Settings["Id"].Value = IdField;
            currentConfig.AppSettings.Settings["HostName"].Value = HostName;
            currentConfig.AppSettings.Settings["Login"].Value = Login;
            currentConfig.AppSettings.Settings["Password"].Value = Password;
            currentConfig.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        /// <summary>
        /// ���������������� ���-������
        /// </summary>
        private void Init()
        {
            var ep = _epStore.GetEndpoints().Select(e => new RmqEndpointViewModelItem(e));
            foreach (var rmqEndpoint in ep)
            {
                EndpointList.Add(rmqEndpoint);
            }

            // ��������� ����������
            HostName = ConfigurationManager.AppSettings["HostName"];
            Login = ConfigurationManager.AppSettings["Login"];
            DllPath = ConfigurationManager.AppSettings["Dll"];
            IdField = ConfigurationManager.AppSettings["Id"];
        }
    }
}