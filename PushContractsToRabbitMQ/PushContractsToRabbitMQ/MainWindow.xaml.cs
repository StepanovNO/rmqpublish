﻿using Itsk.AF.Authentication;
using Itsk.AF.Contracts;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Configuration;
using System.Text.RegularExpressions;

namespace PushContractsToRabbitMQ
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly string _cipherKey = "tYkKibdi221KJLOVqbc5BK+f0FmOYcjZLcITNeq4FU0=";
        private readonly string _initializationVector = "5th76+hSE8vt9twb48uFkA==";
        private Assembly _assembly;
        private List<TypeInfo> _types;

        public MainWindow()
        {
            InitializeComponent();
            passwordBox.Password = ConfigurationManager.AppSettings["Password"];
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxDll.Text == "")
            {
                MessageBox.Show("Введите путь к файлу Dll.");
                return;
            }
            if (textBoxHost.Text == "")
            {
                MessageBox.Show("Введите хост, к примеру \"\\\".");
                return;
            }
            if (textBoxId.Text == "")
            {
                MessageBox.Show("Введите свой Id юзера, можно найти в базе.");
                return;
            }
            if (textBoxLogin.Text == "")
            {
                MessageBox.Show("Введите Login.");
                return;
            }
            if (passwordBox.Password == "")
            {
                MessageBox.Show("Введите пароль.");
                return;
            }
            SendMessage();
        }

        public IConnection GetRabbitMQConnection()
        {
            var factory = new ConnectionFactory
            {
                //HostName = textBoxNameHost.Text,
                //VirtualHost = textBoxHost.Text,
                //UserName = textBoxLogin.Text,
                //Password = passwordBox.Password
            };
            IConnection conn = factory.CreateConnection();
            return conn;
        }

        private void SendMessage()
        {
            try
            {
                //if (CheckJson() == true)
                //{
                //    var conn = GetRabbitMQConnection();
                //    IModel model = conn.CreateModel();
                //    var prop = model.CreateBasicProperties();

                //    var dic = new Dictionary<string, object>();
                //    var uglyJson = Encoding.UTF8.GetBytes(jsonViewerBox.Text);
                //    var aes = new AesEncryptionService(_cipherKey, _initializationVector);

                //    var authtenticationToken = new AuthenticationToken() { User = new UserInfo() };
                //    authtenticationToken.Expires = DateTime.Now.Add(new TimeSpan(1, 0, 0));
                //    authtenticationToken.User.NameWithDomain = WindowsIdentity.GetCurrent().Name;
                //    authtenticationToken.User.DisplayName = WindowsIdentity.GetCurrent().Name;
                //    authtenticationToken.User.Id = Convert.ToInt64(textBoxId.Text);
                //    authtenticationToken.User.Roles = new List<string>() { "" };
                //    authtenticationToken.User.Features = new List<string>() { "" };

                //    var authToken = Convert.ToBase64String(aes.Encrypt(JsonConvert.SerializeObject(authtenticationToken, Formatting.None)));
                //    authToken = "\"" + authToken + "\"";
                //    dic.Add("MessageType", "\"" + tbMessageType.Text + "\"");
                //    dic.Add("authtoken", authToken);
                //    prop.Persistent = true;
                //    prop.DeliveryMode = 2;
                //    prop.Headers = dic;
                //    if (listBoxExchange.SelectedItem == null)
                //    {
                //        var rKey = queueList.SelectedItem.ToString();
                //        model.BasicPublish(exchange: "", routingKey: rKey, basicProperties: prop, body: uglyJson);
                //    }
                //    else if (listBoxExchange.SelectedItem.ToString() == "Itsk.AF.Notification")
                //    {
                //        model.BasicPublish(exchange: listBoxExchange.SelectedItem.ToString(), routingKey: "", basicProperties: prop, body: uglyJson);
                //    }
                //    MessageBox.Show("Сообщение отправлено.");
                //    if (model.IsOpen == true)
                //    {
                //        model.Abort();
                //        conn.Abort();
                //    }
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show("Сообщение не отправлено по причине: \r\n" + ex);
                return;
            }
        }

        private bool CheckJson()
        {
            try
            {
                //var targetType = _assembly.GetType(tbMessageType.Text);

                //Deserialize(Encoding.UTF8.GetBytes(jsonViewerBox.Text), targetType);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"JSON validation failed:{Environment.NewLine}{ex}");
                return false;
            }

        }

        public object Deserialize(byte[] body, Type messageType)
        {
            var message = JsonConvert.DeserializeObject(Encoding.UTF8.GetString(body), messageType, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All,
                FloatParseHandling = FloatParseHandling.Decimal
            });

            return message;
        }


        private void listBoxExchange_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            queueList.SelectedItem = null;
        }

        private List<string> GetFilteredTypeNames(IEnumerable<TypeInfo> types, string filter)
        {
            if (types == null)
            {
                return new List<string>();
            }

            try
            {
                Regex.Match("", filter);
            }
            catch (ArgumentException)
            {
                return types.Select(t => t.Name).ToList();
            }

            return types
                .Where(t => Regex.Match(t.Name, filter, RegexOptions.IgnoreCase).Success)
                .Select(t => t.Name)
                .OrderBy(s => s)
                .ToList();
        }
    }
}