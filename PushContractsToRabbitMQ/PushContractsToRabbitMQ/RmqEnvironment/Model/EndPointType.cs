﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PushContractsToRabbitMQ.RmqEnvironment.Model
{
    /// <summary>
    /// Тип конечной точки брокера
    /// </summary>
    public enum EndPointType
    {

        /// <summary>
        /// Очередь
        /// </summary>
        Queue = 0,

        /// <summary>
        /// Эксчейнджер
        /// </summary>
        Exchanger = 1
    }
}
