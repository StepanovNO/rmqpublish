﻿namespace PushContractsToRabbitMQ.RmqEnvironment.Model
{
    public class RmqEndpoint
    {
        /// <summary>
        /// Имя кончной точки
        /// </summary>
        public string EndpointName { get; set; }

        /// <summary>
        /// Тип конечной точки
        /// </summary>
        public EndPointType EndpointType { get; set; }

        public override bool Equals(object obj)
        {
            var ep = obj as RmqEndpoint;
            if (ep != null)
            {
                return ep.EndpointName == this.EndpointName && this.EndpointType == ep.EndpointType;
            }

            return false;
        }

        public override int GetHashCode()
        {
           return this.EndpointName.GetHashCode() ^ this.EndpointType.GetHashCode();
        }
    }
}
