﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using PushContractsToRabbitMQ.RmqEnvironment.Iface;
using PushContractsToRabbitMQ.RmqEnvironment.Model;

namespace PushContractsToRabbitMQ.RmqEnvironment
{
    public class EndpointStore : IEndpointStore
    {
        /// <summary>
        /// Сериализатор
        /// </summary>
        private XmlSerializer _xseri;

        /// <summary>
        /// Коллекция эндпоинтов
        /// </summary>
        private List<RmqEndpoint> _endpoints;

        /// <summary>
        /// Точки подключения
        /// </summary>
        private const string StoreFileName = "Endpoints.xml";

        /// <summary>
        /// Расположение файла
        /// </summary>
        private string _fileLocation;

        /// <summary>
        /// Хранилище конечных точек подключения
        /// </summary>
        public EndpointStore()
        {
            _xseri = new XmlSerializer(typeof(List<RmqEndpoint>));
            _fileLocation = Path.Combine(Directory.GetCurrentDirectory(), StoreFileName);
        }

        /// <summary>
        /// Получить список доступных конечных точек
        /// </summary>
        /// <returns></returns>
        public IList<RmqEndpoint> GetEndpoints()
        {
            using (var stream = InitEndpointfile())
            {
                try
                {
                    _endpoints = (List<RmqEndpoint>)_xseri.Deserialize(stream);
                }
                catch (Exception e)
                {
                    _endpoints = new List<RmqEndpoint>();
                    return _endpoints;
                }
            }

            return _endpoints;
        }

        /// <summary>
        /// Сохранить точку в существующий файл
        /// </summary>
        /// <param name="endpoint">Точкка подключения</param>
        public void StoreEndpoint(RmqEndpoint endpoint)
        {
            _endpoints.Add(endpoint);
            using (var sw = File.Open(_fileLocation, FileMode.Truncate))
            {
                _xseri.Serialize(sw, _endpoints);
            }
        }

        /// <summary>
        /// Удалить конечную точку подключения
        /// </summary>
        /// <param name="ep"> Точка подключения </param>
        public void RemoveEndpoint(RmqEndpoint ep)
        {
            var idx = _endpoints.IndexOf(ep);
            if (idx != -1)
            {
                _endpoints.RemoveAt(idx);
                using (var sw = File.Open(_fileLocation, FileMode.Truncate))
                {
                    _xseri.Serialize(sw, _endpoints);
                }
            }
        }

        /// <summary>
        /// Инициализировать файл с конечными точками
        /// </summary>
        /// <returns>Поток записи в файл</returns>
        private Stream InitEndpointfile()
        {
            Stream fs;
            if (!File.Exists(_fileLocation))
            {
                fs = File.Create(_fileLocation);
                var writer = new StreamWriter(fs, Encoding.UTF8);
                writer.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
                fs.Position = 0;
            }
            else
            {
                fs = File.Open(_fileLocation, FileMode.Open, FileAccess.ReadWrite);
            }

            return fs;
        }
    }
}
