﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PushContractsToRabbitMQ.RmqEnvironment.Model;

namespace PushContractsToRabbitMQ.RmqEnvironment.Iface
{
    /// <summary>
    /// Хранилище подключений конечных точек
    /// </summary>
    public interface IEndpointStore
    {
        /// <summary>
        /// Список конечных точек
        /// </summary>
        /// <returns> Список конечных точек </returns>
        IList<RmqEndpoint> GetEndpoints();

        /// <summary>
        /// Сохранить конечную точку
        /// </summary>
        /// <param name="endpoint">Конечная точка</param>
        void StoreEndpoint(RmqEndpoint endpoint);

        /// <summary>
        /// Удалить конечную точку
        /// </summary>
        void RemoveEndpoint(RmqEndpoint endpoint);
    }
}
